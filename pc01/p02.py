DA = 21
MA = 1
AA = 2020

def obtener_antiguedad(d, m, a):
    anios = AA - a
    #Casos donde todavia no cumple anios
    if(m > MA or (m == MA and d > DA)):
        anios = anios - 1
    return anios

def mostrar_nuevo_sueldo(salario, antiguedad):
    if(antiguedad > 4):
        salario = 1.15 * salario
    elif(antiguedad >= 1):
        salario = 1.08 * salario
    print("Nuevo salario:", salario)    

def main():
    respuesta = "S"
    while(respuesta != "N"):
        salario = float(input("Ingrese salario: "))
        d1 = int(input("Ingrese dia de inicio: "))
        m1 = int(input("Ingrese mes de inicio: "))
        a1 = int(input("Ingrese anio de inicio: "))
        antiguedad = obtener_antiguedad(d1, m1, a1)
        print("Antiguedad del empleado:", antiguedad)
        mostrar_nuevo_sueldo(salario, antiguedad)
        respuesta = input("Presione 'N' para terminar y otra tecla para continuar: ")
#Pruebas
main()