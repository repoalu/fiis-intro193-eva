n = int(input("Ingrese el valor de n:"))

suma = 0
minimo = -1
i = 0

while(i < n):
    valor = int(input("Ingrese valor: "))
    if(valor < 0):
        break
    
    if(minimo == -1 or valor < minimo):
        minimo = valor
    i = i + 1
    suma = suma + valor

promedio = (suma - minimo) / (i - 1)
print("Promedio:", promedio)

