def es_digito_primo(dig):
    if(dig == 2 or dig == 3 or dig == 5 or dig == 7):
        return True
    else:
        return False
    
def calcular_numero(N):
    suma = 0
    while(N > 0):
        digito = N % 10
        N = N // 10
        if(es_digito_primo(digito) == True):
            suma = suma * 10 + digito 
    return suma

def main():
    resultado = calcular_numero(2836562734)
    print(resultado ** 2)

#Pruebas
main()
