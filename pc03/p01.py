def mostrar_vuelto(monto, denominaciones, cantidades):
    i = len(denominaciones) - 1
    while(monto > denominaciones[0]/2 and i >= 0):
        val_moneda = denominaciones[i]
        cant_monedas = cantidades[i]
        monedas = int(monto / val_moneda)
        
        if(cant_monedas < monedas):
            monedas = cant_monedas
        
        print(f"{monedas} monedas de {val_moneda}")
        monto -= monedas * val_moneda
        i -= 1
    
    if(monto > denominaciones[0]/2):
        print("No es posible dar vuelto")
    
def main():
    den = [10, 20, 50, 100]
    cant = [2, 2, 2, 2]
    monto = 220
    mostrar_vuelto(monto, den, cant)

#Pruebas
main()