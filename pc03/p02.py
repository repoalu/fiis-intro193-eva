def es_diagonal(matriz):
    
    if(len(matriz) != len(matriz[0])):
        #Matriz no es cuadrada
        return False
    else:
        for i in range(len(matriz)):
            for j in range(len(matriz)):
                if(i != j and matriz[i][j] != 0):
                    return False
    return True
                
        
def main():
    matriz1 = [[1, 2, 3], [1, 2, 3]]
    matriz2 = [[1, 0, 0], [0, 1, 0], [0, 0, 5]]
    print(es_diagonal(matriz1))
    print(es_diagonal(matriz2))
#Pruebas
main()