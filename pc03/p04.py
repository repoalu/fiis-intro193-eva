def evaluar_cadena(texto):
    i = 0
    j = len(texto) - 1
    contador = 0
    while(i <= j):
        if(texto[i] != texto[j]):
            contador += 1
        i += 1
        j -= 1
    if(contador <= 1):
        return True
    else:
        return False

def obtener_cadena_max(lista):
    maxlen = len(lista[0])
    cad_max = lista[0]
    for i in range(1, len(lista)):
        longitud = len(lista[i])
        if(longitud > maxlen):
            maxlen = longitud
            cad_max = lista[i]
    return cad_max

def obtener_subcadena(texto):
    subcadenas = []
    for i in range(len(texto)):
        for j in range(i + 1, len(texto)):
            subcadena = texto[i : j]
            if(evaluar_cadena(subcadena) == True):
                subcadenas.append(subcadena)
    return obtener_cadena_max(subcadenas)

def main():
    print(evaluar_cadena("ABC"))
    print(evaluar_cadena("ABBA"))
    print(evaluar_cadena("ABCD"))
    print(obtener_subcadena("ABCDEEDCXX"))

#Pruebas
main()