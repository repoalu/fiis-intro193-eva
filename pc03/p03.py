def mostrar_ganadores(resultados, equipos):
    maxptos = 0
    maxptos2 = 0
    equipomax = 0
    equipomax2 = 0
    for i in range(len(resultados)):
        puntos = 0
        
        for j in range(len(resultados)):
            if(i != j):
                if(resultados[i][j] > resultados[j][i]):
                    puntos += 3
                elif(resultados[i][j] == resultados[j][i]):
                    puntos += 1
        
        print("puntos", i, puntos)
        if(puntos > maxptos):
            maxptos2 = maxptos
            equipomax2 = equipomax
            maxptos = puntos
            equipomax = i
        elif(puntos > maxptos2):
            maxptos2 = puntos
            equipomax2 = i
    print(f"Equipo campeon: {equipos[equipomax]} con {maxptos} puntos")
    print(f"Equipo subcampeon: {equipos[equipomax2]} con {maxptos2} puntos")

def main():
    resultado = [   [0 ,2 ,1],
                    [2 ,0 ,3],
                    [1 ,5 ,0]
                ]
    equipos = ["Equipo A", "Equipo B", "Equipo C"]

    mostrar_ganadores(resultado, equipos)

#Pruebas
main()