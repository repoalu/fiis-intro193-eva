def calcular_viajes(pesos, P, W):
    personas_viaje = 0
    peso_viaje = 0
    cantidad_viajes = 0
    for i in range(len(pesos)):
        if(peso_viaje + pesos[i] > W):
            cantidad_viajes += 1
            peso_viaje = 0
            personas_viaje = 0
        
        personas_viaje += 1
        peso_viaje += pesos[i]      
   
    if(personas_viaje > 0):
        cantidad_viajes += 1

    return cantidad_viajes

def pruebas_calculo():
    pesos = [80, 85, 60]
    P = 3
    W = 180
    print(calcular_viajes(pesos, P, W))

def main():
    pesos = []
    P = int(input("Ingrese capacidad (personas):"))
    W = float(input("Ingrese capacidad (kg):"))
    n = int(input("Ingrese cantidad de personas:"))
    for i in range(n):
        peso = float(input(f"Ingrese peso W{i + 1}:"))
        pesos.append(peso)
    cant = calcular_viajes(pesos, P, W)
    print("Cantidad viajes:", cant)
#Pruebas
pruebas_calculo()