def calcular_expresion(n):
    
    respuesta = 0
    
    for i in range(1, n + 1):
        if(n % 2 != 0):
            respuesta += (i ** 4 + 6 * i ** 3 + 5)
        else:
            respuesta += (i ** 5 + 3 / 20 * i ** 3 + 5)
    return respuesta

def prueba_consecutivos():
    lista1 = []
    for i in range(1, 11):
        lista1.append(i)
    print(lista1)
        
    for i in range(len(lista1)):
        print(calcular_expresion(lista1[i]), end = " ")

def prueba_lista(L):
    for i in range(len(L)):
        print(f"Para el valor {L[i]}: {calcular_expresion(L[i])}")

def main():
    lista = [15, 24, 45, 5, 94]
    prueba_lista(lista)
#Pruebas
main()
