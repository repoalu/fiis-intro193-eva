def cumple_condicion(n):
    encontrado = False
    for i in range(1, n + 1):
        if(i * (i + 1) ==  n):
            encontrado = True
            break
    return encontrado

def cumple_condicion1(n):
    ref = int(n ** 0.5)
    if(ref * (ref + 1) == n):
        return True
    else:
        return False

def main():
    lista = [85, 68, 240 , 272 , 306 , 342 , 380 , 420]
    for i in range(len(lista)):
        print(f"Valor: {lista[i]}. Cumple condicion: {cumple_condicion1(lista[i])}")

#Pruebas
main()